import socket
import random

# Puerto: 8080

mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mySocket.bind(('localhost', 8080))

mySocket.listen(5)

try:
    while True:
        print("Waiting for connections")
        (recvSocket, address) = mySocket.accept()
        print("HTTP request received:")
        received = recvSocket.recv(2048)
        print(received)
        pages = [
            'https://es.wikipedia.org',
            'http://localhost:8080/',
            'https://google.es',
            'https://www.aulavirtual.urjc.es',
            'https://labs.etsit.urjc.es/',
            'https://www.nytimes.com/',
            'https://www.bbc.co.uk/',
            'https://www.nasa.gov/',
            'https://www.amazon.com/',
            'https://www.netflix.com/'
        ]
        recvSocket.send(b"HTTP/1.1 301 Moved Permanently\r\n\r\n" +
        b"<html><body><h1>Hi!</h1>" + b"<h2>You will be redirected to one of the pages we have in our database as soon as possible. Goodbye :)</h2>" +
        b"<meta http-equiv='refresh' content='3; url=" + bytes(random.choice(pages), 'utf-8') + b"'>" +
        b"</body></html>" + b"\r\n")
        recvSocket.close()

except KeyboardInterrupt:
    print("Closing binded socket")
    mySocket.close()

